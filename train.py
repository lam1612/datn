from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import accuracy
from surprise.model_selection import train_test_split

from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV
import sys

number_factor 	= int(sys.argv[1])
epoch			= int(sys.argv[2])
learning_rate	= float(sys.argv[3])
regular 		= float(sys.argv[4])

reader = Reader(line_format='user item rating', sep=',', rating_scale=(0.,1.))

# and split it into 3 folds for cross-validation.
data = Dataset.load_from_file('../e.csv', reader=reader)
trainset, testset = train_test_split(data, test_size=.2)

# We'll use the famous SVD algorithm.
algo = SVD(n_factors = number_factor, n_epochs = epoch, lr_all = learning_rate, reg_all = regular)

# Train the algorithm on the trainset, and predict ratings for the testset
algo.fit(trainset)
predictions = algo.test(testset)

# Then compute RMSE
accuracy.rmse(predictions)