from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import accuracy
from surprise.model_selection import train_test_split

from surprise.model_selection import cross_validate
from surprise.model_selection import GridSearchCV

reader = Reader(line_format='user item rating', sep=',', rating_scale=(0.,1.))

# and split it into 3 folds for cross-validation.
data = Dataset.load_from_file('e.csv', reader=reader)
trainset, testset = train_test_split(data, test_size=.2)

# We'll use the famous SVD algorithm.
algo = SVD(n_factors = 10, n_epochs = 10, lr_all = 0.02, reg_all = 0.01)

# Train the algorithm on the trainset, and predict ratings for the testset
algo.fit(trainset)
predictions = algo.test(testset)

# Then compute RMSE
# accuracy.rmse(predictions)

# param_grid = {'n_epochs': [5, 10, 15, 20, 30, 40, 50], 'lr_all': [0.002, 0.004, 0.006, 0.008, 0.01],
#               'reg_all': [0.01, 0.02, 0.03, 0.04, 0.05], 'n_factors':[10, 20, 30, 40, 50]}
# gs = GridSearchCV(SVD, param_grid, measures=['rmse', 'mae'], cv=3)

# gs.fit(data)

# # best RMSE score
# print(gs.best_score['rmse'])

# # combination of parameters that gave the best RMSE score
# print(gs.best_params['rmse'])