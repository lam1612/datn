<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class StudentLevel extends Model
{
    protected $table = 'lms_level_student';

    public static function getData($course_id) {
    	$results = self::select('user_id', 'section', 'level')
                    ->where('course_id', '=', $course_id)
                    ->get();
    	return $results;
    }

    public static function getDataPaginate($course_id) {
        $results = self::select('user_id', 'section', 'level')
                        ->where('course_id', '=', $course_id)
                        ->paginate(10);

        return $results;
    }

    public static function getUser($course_id) {
        $results = self::select('user_id')
                        ->where('course_id', '=', $course_id)
                        ->paginate(10);

        return $results;
    }

}
