<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Concept;
use App\Course;

class CourseController extends Controller
{
    public function index(Request $request) {
    	return view('course_index', ['id' => $request->id]);
    }
}
