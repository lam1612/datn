<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PracticResult;
use App\ReviseResult;
use DB;

class EvalueController extends Controller
{
	CONST AVERAGE 		= 2;
	CONST EASY 			= 1;
	CONST HARD 			= 3;

	CONST hard_high_g	 		= 0.35;
	CONST hard_average_g		= 0.76;
	CONST hard_low_g			= 0.88;
	CONST average_high_g		= 0.25;
	CONST average_average_g		= 0.39;
	CONST average_low_g			= 0.71;
	CONST easy_high_g			= 0.11;
	CONST easy_average_g		= 0.23;
	CONST easy_low_g			= 0.43;

	CONST hard_high_s	 		= 0.21;
	CONST hard_average_s		= 0.09;
	CONST hard_low_s			= 0.01;
	CONST average_high_s		= 0.61;
	CONST average_average_s		= 0.42;
	CONST average_low_s			= 0.19;
	CONST easy_high_s			= 0.89;
	CONST easy_average_s		= 0.74;
	CONST easy_low_s			= 0.47;

    public function index(Request $request) {
    	return view('evalue', ['id'=>$request->id]);
    }

    public function evalue(Request $request) {
    	$id = $request->id;
    	$config_G = array();

    	$df = fopen('config_G.txt', 'r');
		// while (!feof($df)) {
		//     $line = fgets($df);
		    
		//     $config = explode("\t", $line);
		//     // dd($config);
		//     $config_G[$config[0]][$config[1]] = floatval($config[2]);
		//     // dd($config_G[$config[0]][$config[1]]);
		// }
		// fclose($df);

		// $df = fopen('config_S.txt', 'r');
		// $config_S = array();
		// while (!feof($df)) {
		//     $line = fgets($df);
		//     $config = explode("\t", $line);
		//     $config_S[$config[0]][$config[1]] = floatval($config[2]);
		// }
		// fclose($df);
    // 	$users = DB::select("SELECT user_id FROM vh_web_doan.lms_revise_result where course_id=13 
				// and user_id not in (22,1, 511) group by user_id");

    // 	$revises = DB::select("SELECT revise_id FROM vh_web_doan.lms_revise_result where course_id=13 
				// and user_id not in (22,1, 511) group by revise_id");

    // 	foreach($users as $user) {
    // 		foreach($revises as $revise) {
    // 			$results = DB::select("SELECT user_id, revise_id, section, coc_question.id, is_correct ,coc_question.level_id 
				// 	FROM lms_revise_result, coc_question, lms_course_modules
				// 	where lms_revise_result.question_id = coc_question.id
				// 	and lms_course_modules.instance = lms_revise_result.revise_id
				// 	and lms_revise_result.user_id = ".$user->id.
				// 	"and lms_revise_result.revise_id =".$revise_id.
				// 	"and lms_course_modules.course_id=13 and user_id not in (1, 22, 511)")
    // 		}
    // 	}
    // 	dd($users[0]->user_id);

		$results = DB::select("SELECT user_id, revise_id, section, coc_question.id, is_correct ,coc_question.level_id 
			FROM lms_revise_result, coc_question, lms_course_modules
			where lms_revise_result.question_id = coc_question.id
			and lms_course_modules.instance = lms_revise_result.revise_id
			and lms_revise_result.user_id = 253
			and lms_revise_result.revise_id =1033
			and lms_course_modules.course_id=13 and user_id not in (1, 22, 511)");

		$user_level = DB::select("SELECT level FROM lms_level_student
			where user_id = 253 and section = 1");
		// dd($results);

		// dd($user_level);
		$user_level = $user_level[0]->level;

		foreach($results as $result) {
			if ($result->is_correct == 1) {
				if($result->level_id == 1) {
					if($user_level < 0.4) {
						$a = ($user_level * (1-self::easy_low_s));
						$b = $a + (1-$user_level) * self::easy_low_g;
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = ($user_level * (1-self::easy_average_s));
						$b = $a + (1-$user_level) * self::easy_average_g;
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = ($user_level * (1-self::easy_high_s));
						$b = $a + (1-$user_level) * self::easy_high_g;
						$user_level = $a / $b;
					}
				} elseif ($result->level_id == 2) {
					if($user_level < 0.4) {
						$a = ($user_level * (1-self::average_low_s));
						$b = $a + (1-$user_level) * self::average_low_g;
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = ($user_level * (1-self::average_average_s));
						$b = $a + (1-$user_level) * self::average_average_g;
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = ($user_level * (1-self::average_high_s));
						$b = $a + (1-$user_level) * self::average_high_g;
						$user_level = $a / $b;
					}

				} elseif ($result->level_id == 3) {
					if($user_level < 0.4) {
						$a = ($user_level * (1-self::hard_low_s));
						$b = $a + (1-$user_level) * self::hard_low_g;
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = ($user_level * (1-self::hard_average_s));
						$b = $a + (1-$user_level) * self::hard_average_g;
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = ($user_level * (1-self::hard_high_s));
						$b = $a + (1-$user_level) * self::hard_high_g;
						$user_level = $a / $b;
					}
				}
			} else {
				if($result->level_id == 1) {
					if($user_level < 0.4) {
						$a = $user_level * self::easy_low_s;
						$b = $a + (1-$user_level) * (1-self::easy_low_g);
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = $user_level * self::easy_average_s;
						$b = $a + (1-$user_level) * (1-self::easy_average_g);
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = $user_level * self::easy_high_s;
						$b = $a + (1-$user_level) * (1-self::easy_high_g);
						$user_level = $a / $b;
					}
				} elseif ($result->level_id == 2) {
					if($user_level < 0.4) {
						$a = $user_level * self::average_low_s;
						$b = $a + (1-$user_level) * (1-self::average_low_g);
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = $user_level * self::average_average_s;
						$b = $a + (1-$user_level) * (1-self::average_average_g);
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = $user_level * self::average_high_s;
						$b = $a + (1-$user_level) * (1-self::average_high_g);
						$user_level = $a / $b;
					}

				} elseif ($result->level_id == 3) {
					if($user_level < 0.4) {
						$a = $user_level * self::hard_low_s;
						$b = $a + (1-$user_level) * (1-self::hard_low_g);
						$user_level = $a / $b;
					} elseif (($user_level >= 0.4) and ($user_level <= 0.7)) {
						$a = $user_level * self::hard_average_s;
						$b = $a + (1-$user_level) * (1-self::hard_average_g);
						$user_level = $a / $b;
					} elseif(($user_level > 0.7)) {
						$a = $user_level * self::hard_high_s;
						$b = $a + (1-$user_level) * (1-self::hard_high_g);
						$user_level = $a / $b;
					}
				}
			}
		}
		dd($user_level); 
    }
}
