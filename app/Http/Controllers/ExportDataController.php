<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentLevel;

class ExportDataController extends Controller
{
	public function index(Request $request) {
		$id = $request->id;

		if(file_exists('data'.$id.'.csv')) {
			$datas = StudentLevel::getDataPaginate($id);
		} else {
			$datas = [];
		}

		return view('export', ['id'=>$id, 'datas' => $datas]);
	}

    public function export(Request $request) {
    	$course_id = $request->course_id;

    	$results = StudentLevel::getData($course_id);
    	// dd($results);
    	$file_name = 'data'.$course_id. '.csv';
    	$df = fopen($file_name, 'w+');

	   
	    foreach ($results as $row) {
	       fputcsv($df, $row->toArray());
	    }
	    fclose($df);

	    return redirect()->route('courses.get_export', '12')->with([ 'message' => 'Export thành công'] );
    }
}
