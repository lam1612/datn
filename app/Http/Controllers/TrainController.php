<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect,Response;

class TrainController extends Controller
{
    public function index(Request $request) {
    	return view('train', ['id'=>$request->id]);
    }

    public function training(Request $request) {
    	$id 			= $request->id;
    	$number_factor 	= $request->number_factor;
    	$epoch 			= $request->epoch;
    	$learning_rate 	= $request->learning_rate;
    	$regular 		= $request->regular;

    	if(file_exists('data'.$id.'.csv')) {	
    		
    		$time_start = microtime(true); 

	    	exec('python ../train.py'.' '.$number_factor.' '.$epoch.' '.$learning_rate.' ' .$regular, $output, $ret_code);
	    	
	    	$time_end = microtime(true);

			$rmse = $output;
			$execution_time = ($time_end - $time_start);

			return response()->json(['rmse' => $rmse, 'excute_time' => $execution_time]);
		} else {
			return response()->json(['message' => 'Chưa export dữ liệu']);
		}
    }
}
