from collections import defaultdict

from surprise import SVD
from surprise import Dataset
from surprise import Reader
from surprise import accuracy


def get_top_n(predictions, n=10):

    # First map the predictions to each student.
    top_n = defaultdict(list)
    for uid, iid, true_r, est, _ in predictions:
        top_n[uid].append((iid, est))

    # Then sort the predictions for each user and retrieve the k highest ones.
    for uid, user_ratings in top_n.items():
        user_ratings.sort(key=lambda x: x[1], reverse=True)
        top_n[uid] = user_ratings[:n]

    return top_n

reader = Reader(line_format='user item rating', sep=',', rating_scale=(0.,1.))
data = Dataset.load_from_file('e.csv', reader=reader)
trainset = data.build_full_trainset()
algo = SVD()
algo.fit(trainset)

# Than predict ratings for all pairs (u, i) that are NOT in the training set.
testset = trainset.build_anti_testset()
predictions = algo.test(testset)
# print(accuracy.rmse(predictions))
# print(predictions)
top_n = get_top_n(predictions, n=5)

# Print the recommended items for each user
for uid, user_ratings in top_n.items():
    print(uid, [iid for (iid, _) in user_ratings])