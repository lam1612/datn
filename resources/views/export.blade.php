@extends('layouts.master1')

@section('title', 'Course detail')

@section('content')

<div class="container">
    <div class="card bg-light mt-3">
      @if (Session::has('message'))
        {{ session()->get('message') }}
      @endif
      <form action="{{ route('courses.post_export', '$id') }}" method="POST">
          <div class="card-header">
            @csrf
            Export new data from database
          </div>
          <div class="card-body">
            <button type="submit" class="btn btn-warning">
              Export Data
            </button>
          </div>
      </form>
    </div>
</div>
<br/>
<hr/>

<div class="container">
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="row">#</th>
      <th scope="col">User</th>
      <th scope="col">Lesson</th>
      <th scope="col">Level of student</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($datas as $data)
    <tr>
      <th scope="row"></th>
      <td>{{$data->user_id}}</td>
      <td>{{$data->section}}</td>
      <td>{{$data->level}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $datas->links() }}
</div>
@endsection