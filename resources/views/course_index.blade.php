@extends('layouts.master1')

@section('title', 'Course detail')

@section('content')

<div class="container">
    <div class="card bg-light mt-3">
      @if (Session::has('message'))
        {{ session()->get('message') }}
      @endif
      <form action="{{ route('courses.post_export', '$id') }}" method="POST">
          <div class="card-header">
            @csrf
            Export data from data base to train
          </div>
          <div class="card-body">
            <button type="submit" class="btn btn-warning">
              Export Data
            </button>
          </div>
      </form>
    </div>
</div>
@endsection