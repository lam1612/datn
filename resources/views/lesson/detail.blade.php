@extends('layouts.course_master')

@section('title', 'Course detail')

@section('content')

  <div class="col-md-8 course-main">
    <iframe width="100%" height="75%" src="https://www.youtube.com/embed/vja0332vJxA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
  </div>

@include('partials.recommend')
@endsection
