@extends('layouts.master1')

@section('title', 'Course detail')

@section('content')
  <div class="col-md-8 course-main">
        <div id="message" class="">
          
        </div>
        <h1 class="border-bottom">
          Huấn luyện mô hình
        </h1>
        <form>
          @csrf
          <label for="sel1"><b>Tham số dùng cho giải thuật</b></label>
          <div class="row form-group">
              <label class="control-label col-md-3" for="subcategory_id">Number factor K</label>
              <input class="form-control col-md-2" type="number" value="0" id="example-number-input"
              min=0 max=1000 step=1 name="number_factor" required>
              <label  class="control-label col-md-2" for="type_id">Max n_epochs</label>
              <input class="form-control col-md-2" type="number" value="0" id="example-number-input"
              min=0 max=1000 step=1 name="epoch" required>
          </div>  
          <div class="row form-group">
              <label class="control-label col-md-3" for="subcategory_id">Learning rate</label>
              <input class="form-control col-md-2" type="number" value="0" id="example-number-input"
                min=0 max=1 step=0.001 name="learning_rate" required>
              <label  class="control-label col-md-2" for="type_id">Regularization</label>
              <input class="form-control col-md-2" type="number" value="0" id="example-number-input"
                min=0 max=1 step=0.005 name="regular" required> 
          </div>  
          <div>
            <button type="button" class="btn btn-info" id="submit">Dự đoán</button>
          </div>
        </form>
        </div><!-- /.course-post -->
    @include('partials.sidebar')
@endsection
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
  $(document).ready(function () {
    $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
    $("#submit").click(function(e){
        e.preventDefault();
        var number_factor= $("input[name=number_factor]").val();
        var epoch = $("input[name=epoch]").val();
        var learning_rate = $("input[name=learning_rate]").val();
        var regular = $("input[name=regular]").val();
        $.ajax({
           type:'POST',
           url:"{{route('courses.post_train', $id)}}",
           dataType: 'json',
           data:{
              number_factor:number_factor, 
              epoch:epoch, 
              learning_rate:learning_rate,
              regular:regular,
              _token: '{{csrf_token()}}'
            },
           success:function(data){
              if(data["excute_time"]) {
                var message = $("#message");
                message.html("");
                message.removeClass('alert alert-danger');

                var mrse = $("#rmse");
                var execute = $("#execute");
                
                mrse.val(data["rmse"]);
                execute.val(data["excute_time"]);

              } else if(data.message) {
                var message = $("#message");
                message.html("");
                message.addClass('alert alert-danger');
                message.append(data.message);
              }
            }
        });
    });
  });
</script>
