@extends('layouts.master')

@section('title', 'Course detail')

@section('content')
  <div class="col-md-8 course-main">
    <div class="course_name">
          <h1 class="border-bottom">
            {{$course->name}}
          </h1>
    </div>
          <hr/>
          <div class="course-post">
            <div id="header-fixed">
                <h3 class="">Nội dung khoá học</h3>
                <p class="course-post-meta">November 1, 2019 by <a href="#">Nguyễn Quốc Lâm</a></p>
            </div>
            <div id="accordion">
            @foreach($concepts as $concept)
                <div class="card">
                    <div class="card-header" id="heading{{$concept->id}}">
                        <h4 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$concept->id}}" aria-expanded="true" aria-controls="collapse{{$concept->id}}">
                            <h5>{{$concept->name}}</h5>
                            </button>
                        </h4>
                    </div>
                    
                    <div id="collapse{{$concept->id}}" class="collapse show" aria-labelledby="heading{{$concept->id}}" data-parent="#accordion">
                        <div class="card-body">
                        <ul>
                          @foreach($concept->lessons as $lesson)
                            <li>
                              <div>
                                <a href="{{route('courses.lessons.show', ['slug' => $course->slug, 'id' => $lesson->id])}}">
                                  <h6>
                                    <span>{{$lesson->name}}       </span>
                                    <i class="fa fa-play-circle" aria-hidden="true"></i>
                                  </h6>
                                </a>
                              </div>
                            </li>
                          @endforeach()
                        <ul>
                        </div>
                    </div>
                </div>
            @endforeach
            <div>
                <hr/>
            </div>
            {{ $concepts->links() }}
            
          </div>
        </div><!-- /.course-post -->
      </div><!-- /.course-main -->
@include('partials.sidebar')
@endsection
