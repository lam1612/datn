<!doctype html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
	    <!-- <link rel="icon" href="../../../../favicon.ico"> -->

	    <title>Course</title>
		  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
	    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
	    <link href="{{ asset('css/banner/banner.css') }}" rel="stylesheet" />
	    <link href="{{ asset('css/header/header.css') }}" rel="stylesheet" />
	    <link href="{{ asset('css/footer/footer.css') }}" rel="stylesheet" />
      <link href="{{ asset('css/sidebar/sidebar.css') }}" rel="stylesheet" />
      <link href="{{ asset('css/course/index.css') }}" rel="stylesheet" />
      <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
	    <!-- Custom styles for this template -->
	    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
	</head>

<body>

    <div class="">
    	@include('partials.header')
      
      @include('partials.menu')
    </div>
    <main role="main" class="container">
      	<div class="row">
        	@yield('content')
      	</div>
    </main><!-- /.container -->
	@include('partials.footer')
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <!-- <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script> -->
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/holder.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>s
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    //      
    </script>
  </body>
</html>
