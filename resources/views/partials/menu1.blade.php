<nav class="course-nav navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <ul class="navbar-nav mr-auto mt-3 mt-lg-0 nav-pills nav-fill">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('home')}}">Trang chủ</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('courses.get_evalue', $id)}}">Đánh giá trình độ học sinh</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('courses.get_export', $id)}}">Export dữ liệu</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('courses.get_train', $id)}}">Huấn luyện</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('courses.get_predict', $id)}}">Dự đoán</a>
      </li>
    </ul>
   <!--  <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->
  </div>
</nav>