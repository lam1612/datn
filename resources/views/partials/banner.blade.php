<div class="banner p-3 p-md-5 text-white rounded">
    <div class="col-md-6 px-0">
        <h1 class="display-4 font-italic">Học, học nữa, học mãi</h1>
        <p class="lead my-3">Trường học có thể hô biến những người thắng và người bại, nhưng cuộc sống thì không – Bill Gate –</p>
        <hr/>
        <p class="lead my-3">Giáo dục là vũ khí mạnh nhất mà người ta có thể sử dụng để thay đổi cả thế giới.
		– N. Mandela –</p>
		<hr/>
		<p class="lead my-3">Muốn xây dựng đất nước, trước hết phải phát triển giáo dục. Muốn trị nước phải trọng dụng người tài – Chiếu Lập Học –</p>
        <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
    </div>
</div>