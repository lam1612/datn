<header class="course-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4 pt-1">
            <a class="text-muted" href="#"></a>
        </div>
        <div class="col-4 text-center">
            <a class="course-header-logo text-dark" href="#">Chương trình thử nghiệm</a>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center">
            <a class="text-muted" href="#">
              	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mx-3">></svg>
            </a>
            <a class="btn btn-sm btn-outline-secondary" href="#"></a>
        </div>
    </div>
</header>