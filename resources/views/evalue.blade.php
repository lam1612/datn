@extends('layouts.master1')

@section('title', 'Course detail')

@section('content')

<div class="container">
    <div class="card bg-light mt-3">
      @if (Session::has('message'))
        {{ session()->get('message') }}
      @endif
      <form action="{{ route('courses.post_evalue', '$id') }}" method="POST">
          <div class="card-header">
            @csrf
            Tính toán trình độ của học sinh
          </div>
          <div class="card-body">
            <button type="submit" class="btn btn-warning">
              Tính toán 
            </button>
          </div>
      </form>
    </div>
</div>
<br/>
<hr/>

<div class="container">
	<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="row">#</th>
      <th scope="col">User</th>
      <th scope="col">Lesson</th>
      <th scope="col">Level of student</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      
    </tr>
  </tbody>
</table>
</div>
@endsection