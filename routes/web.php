<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    Route::get('/', 'HomeController@index')->name('home');

    Route::prefix('{id}')->name('courses.')->group( function() {
        Route::get('/', 'CourseController@index')->name('index');

        Route::get('evalue', 'EvalueController@index')->name('get_evalue');
        Route::post('evalue', 'EvalueDataController@evalue')->name('post_evalue');

        Route::get('export', 'ExportDataController@index')->name('get_export');
        Route::post('export', 'ExportDataController@export')->name('post_export');

        Route::get('train', 'TrainController@index')->name('get_train');
        Route::post('train', 'TrainController@training')->name('post_train');

        Route::get('predict', 'ExportDataController@index')->name('get_predict');
        Route::post('predict', 'ExportDataController@export')->name('post_predict');
    });
